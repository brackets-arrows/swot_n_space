from common import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'analyze',
        'USER': 'analyze',
        'HOST': 'localhost',
        'PASSWORD': 'basajSildIobgu',
        'TEST_CHARSET': "utf8",
    }
}
ALLOWED_HOSTS = ["v9659.hosted-by-vdsina.ru"]

STATIC_ROOT = "/root/swot_n_space/static/"
