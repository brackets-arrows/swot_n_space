from django.conf.urls import patterns, include, url, static
from django.conf import settings
from django.contrib import admin

from sns.views import *


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),

    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': "login.html"}, name="login"),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name="logout"),
    url(r'^register/$', RegisterView.as_view(), name="register"),

    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^my_companies/', CompaniesList.as_view(), name="companies_list"),
    url(r'^company/save$', save_company, name="company_save"),
    url(r'^analyze/save$', save_analyze, name="analyze_save"),
    url(r'^company/(\d+)?$', lambda request, id_comp: company(request, id_comp), name="company"),
    url(r'^analyze/new/(\w+)/(\d+)/company/(\d+)$',
        lambda request, analyze_type, step, company_id: analyze(request, analyze_type, None, step, company_id),
        name="analyze_type"),
    url(r'^analyze/(\d+)/(\d+)?$', lambda request, analyze_id, step: analyze(request, None, analyze_id, step), name="analyze"),
) + static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
  + static.static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
