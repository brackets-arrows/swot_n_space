# -*- coding: utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse

import jsonfield
from django.contrib.auth.models import User
from collections import OrderedDict


class Company(models.Model):
    owner = models.ForeignKey(User)
    name = models.CharField(max_length=256, verbose_name=u"Название")
    description = models.TextField(verbose_name=u"Общее описание", blank=True, default="")

    def get_absolute_url(self):
        return reverse("company", args=[self.id])

    def __unicode__(self):
        return self.name


class Analysis(models.Model):
    create_date = models.DateField(auto_now=True)
    update_date = models.DateField(auto_now_add=True)
    company = models.ForeignKey('Company')
    type = models.CharField(max_length=5, choices=(('SWOT', 'SWOT'), ('SPACE', 'SPACE')))
    json_data = jsonfield.JSONField()

    def get_absolute_url(self):
        return reverse("analyze", args=[self.id])

    def get_absolute_urls(self):
        return {step: reverse("analyze", args=[self.id, step]) for step in [1, 2, 3, 4]}

    def __unicode__(self):
        return u"%s %s" % (self.type, self.company.name)


TELESCOPIC = OrderedDict([
    ('Technological', 'Технологические достижения'),
    ('Economic', 'Экономические соображения'),
    ('Legal', 'Правовые и нормативные требования'),
    ('Ecological', 'Экологические и природоохранные вопросы'),
    ('Sociological', 'Социологические тенденции'),
    ('Competition', 'Конкуренция'),
    ('Organisational', 'Организационная культура'),
    ('Portfolio', 'Анализ портфолио'),
    ('International', 'Международные вопросы'),
    ('Cost', 'Экономическая эффективность и структуры затрат'),
    ('OrganisationalCore', 'Организационные ключевые компетенции и возможности'),
    ('Buyers', 'Покупатели'),
    ('Suppliers', 'Поставщики'),
    ('Electronic', 'Электронная коммерция'),
    ('Resource', 'Аудит ресурсов'),
    ('Value', 'Ценностная цепь'),
    ('Alliances', 'Союзы (в том числе партнерств, сетей и совместных предприятий)'),
    ('Total', 'Общее управление качеством'),
    ('Industry', 'Ключеваые факторы успеха в данной индустрии'),
    ('OrganisationalStruct', 'Организационная структура'),
    ('New', 'Новые участники'),
    ('Substitute', 'Товары-заменители и услуги')
])

SWOT_1 = OrderedDict([
    ('weight', 'Вес'),
    ('S', 'Силы'),
    ('W', 'Слабости'),
    ('O', 'Возможности'),
    ('T', 'Угрозы')
])

SWOT_2 = OrderedDict([
    ('S', 'Силы'),
    ('W', 'Слабости'),
    ('O', 'Возможности'),
    ('T', 'Угрозы')
])

SWOT_2_col = [
    {'id': 'factor', 'name': 'Фактор'},
    {'id': 'weight', 'name': 'Вес'},
    {'id': 'mark', 'name': 'Оценка'},
    {'id': 'total_mark', 'name': 'Итоговая оценка'},
]

SWOT_3_tables = [
    {'first': 'S', 'second': 'O', 'first_name': 'Strengths', 'second_name': 'Opportunities',
     'name': 'Strengths/Opportunities'},
    {'first': 'W', 'second': 'O', 'first_name': 'Weaknesses', 'second_name': 'Opportunities',
     'name': 'Weaknesses/Opportunities '},
    {'first': 'S', 'second': 'T', 'first_name': 'Strengths', 'second_name': 'Threats', 'name': 'Strengths/Threats'},
    {'first': 'W', 'second': 'T', 'first_name': 'Weaknesses', 'second_name': 'Threats', 'name': 'Weaknesses/Threats'}
]

SPACE_1 = [{
               'group_name': 'КОНКУРЕНТНОЕ ПРЕИМУЩЕСТВО',
               'group_id': 'competive',
               'marks': [-1, -2, -3, -4, -5, -6],
               'factors': [
                   {'factor_name': 'Доля рынка', 'default_weight': 0.17, 'factor_id': 'market_share'},
                   {'factor_name': 'Качество продукта', 'default_weight': 0.17, 'factor_id': 'quality'},
                   {'factor_name': 'Приверженность потребителей', 'default_weight': 0.17, 'factor_id': 'loyality'},
                   {'factor_name': 'Использование произ-х мощностей', 'default_weight': 0.17, 'factor_id': 'capacity'},
                   {'factor_name': 'Технологическое know-how(объем знаний)', 'default_weight': 0.17,
                    'factor_id': 'knowhow_knowledge'},
                   {'factor_name': 'Степень вертикальной интеграции ', 'default_weight': 0.15,
                    'factor_id': 'vert_integr'},
               ]
           },
           {
               'group_name': 'ФИНАНСОВОЕ ПОЛОЖЕНИЕ',
               'group_id': 'finance',
               'marks': [1, 2, 3, 4, 5, 6],
               'factors': [
                   {'factor_name': 'Отдача на вложения', 'default_weight': 0.14, 'factor_id': 'return_invest'},
                   {'factor_name': 'Эффект финансового рычага', 'default_weight': 0.14, 'factor_id': 'leverage'},
                   {'factor_name': 'Ликвидность', 'default_weight': 0.14, 'factor_id': 'liquid'},
                   {'factor_name': 'Степень удовлетворения потребности в капитале', 'default_weight': 0.14,
                    'factor_id': 'satisfy_capital'},
                   {'factor_name': 'Поток платежей в пользу фирмы', 'default_weight': 0.14,
                    'factor_id': 'stream_payments'},
                   {'factor_name': 'Простота выхода с рынка', 'default_weight': 0.14, 'factor_id': 'easy_exit'},
                   {'factor_name': 'Рискованность бизнеса', 'default_weight': 0.16, 'factor_id': 'risk'},
               ]
           },
           {
               'group_name': 'ПРИВЛЕКАТЕЛЬНОСТЬ ОТРАСЛИ',
               'group_id': 'industry',
               'marks': [1, 2, 3, 4, 5, 6],
               'factors': [
                   {'factor_name': 'Потенциал роста', 'default_weight': 0.13, 'factor_id': 'potential_growth'},
                   {'factor_name': 'Потенциальная прибыльность', 'default_weight': 0.13,
                    'factor_id': 'potential_profit'},
                   {'factor_name': 'Финансовая стабильность', 'default_weight': 0.13, 'factor_id': 'stability'},
                   {'factor_name': 'Технологическое know-how(сложность технологий)', 'default_weight': 0.13,
                    'factor_id': 'knowhow_hard'},
                   {'factor_name': 'Использование ресурсов', 'default_weight': 0.13, 'factor_id': 'resourse'},
                   {'factor_name': 'Капиталоемкость', 'default_weight': 0.13, 'factor_id': 'capital_intensity'},
                   {'factor_name': 'Легкость вхождения на рынок', 'default_weight': 0.13, 'factor_id': 'easy_enter'},
                   {'factor_name': 'Трудоемкость', 'default_weight': 0.17, 'factor_id': 'laboriousness'},
               ]
           },
           {
               'group_name': 'СТАБИЛЬНОСТЬ ВНЕШНЕЙ СРЕДЫ',
               'group_id': 'stability',
               'marks': [-1, -2, -3, -4, -5, -6],
               'factors': [
                   {'factor_name': 'Технологические изменения', 'default_weight': 0.17, 'factor_id': 'texh_changes'},
                   {'factor_name': 'Темп инфляции', 'default_weight': 0.17, 'factor_id': 'inflation'},
                   {'factor_name': 'Вариация спроса', 'default_weight': 0.17, 'factor_id': 'var_demand'},
                   {'factor_name': 'Разброс цен конкурирующих продуктов', 'default_weight': 0.17,
                    'factor_id': 'var_prices_competing'},
                   {'factor_name': 'Давление конкурентов', 'default_weight': 0.17, 'factor_id': 'competitive_press'},
                   {'factor_name': 'Эластичность спроса', 'default_weight': 0.15, 'factor_id': 'elasticity_demand'},
               ]
           }]