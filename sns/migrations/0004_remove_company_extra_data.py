# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sns', '0003_auto_20150304_1158'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='extra_data',
        ),
    ]
