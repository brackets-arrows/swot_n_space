/**
 * Created with PyCharm.
 * User: alef
 * Date: 05.03.15
 * Time: 18:40
 * To change this template use File | Settings | File Templates.
 */
var space = {};

space.reCalcAbsMark = function (factorId) {
    var factorWeight = $("input[data-factor-id='" + factorId + "']");
    var factorVal = $("select[data-factor-id='" + factorId + "'] option:selected");
    var factorAbsVal = $("p[data-factor-id='" + factorId + "']");
    var absVal = parseInt($(factorVal).val()) * parseFloat($(factorWeight).val());
    var finalVal = absVal.toFixed(2);
    factorAbsVal.html(finalVal);
    return parseFloat(finalVal);
};

space.checkSumWeight = function (groupId) {
    return Math.abs(space.sumWeight(groupId) - 1) < 0.01;
};

space.sumWeight = function (groupId) {
    var group = $('input[data-group-id="' + groupId + '"]');
    var weights = group.filter('input[data-type="weight"]');
    var factorWeightSum = 0;
    weights.each(function () {
        factorWeightSum += parseFloat($(this).val()) || 0;
    });
    return factorWeightSum;
};

space.setCalcWeight = function (groupId) {
    $('p[data-group-id="' + groupId + '"][data-type="calc_weight"]')
        .html(space.sumWeight(groupId).toFixed(2));
};

space.setGroupMark = function (groupId) {
    var tableToMark = $("table#"+groupId),
        dataRows =  tableToMark.find("tr.data-row"),
        factorsCount = dataRows.length,
        groupMark = 0,
        groupMarkElement = $(".group_mark[data-group-id="+groupId+"]");
    tableToMark.find("tr.data-row").each(function(){
        groupMark += space.reCalcAbsMark($(this).attr("data-factor-id"));
    });
    groupMarkElement.text((groupMark/factorsCount).toFixed(3));
};

space.makeTable = function (jsonData) {
    jsonData = JSON.parse(jsonData);
    console.log(jsonData);
    $("input[type='text'],select").each(function () {
        var jObj = $(this);
        jObj.val(jsonData[jObj.attr('data-group-id')][jObj.attr('data-factor-id')][jObj.attr('data-type')]);
    });
    $('p[data-type="calc_weight"]').each(function () {
        space.setCalcWeight($(this).attr('data-group-id'));
        space.setGroupMark($(this).attr('data-group-id'));
    });
    $('p[data-type="calc_mark"]').each(function () {
        space.reCalcAbsMark($(this).attr('data-factor-id'));
    });
};
space.getTable = function () {
    var isValid = true;
    $('p[data-type="calc_weight"]').each(function () {
        if (!space.checkSumWeight($(this).attr('data-group-id'))) {
            isValid = false;
        }
    });
    var jsonData = {};
    $("input[type='text'],select").each(function () {
        var jObj = $(this);
        var groupId = jObj.attr('data-group-id');
        var factorId = jObj.attr('data-factor-id');
        var typeVal = jObj.attr('data-type');

        if (!(groupId in jsonData)) {
            jsonData[groupId] = {};
        }
        if (!(factorId in jsonData[groupId])) {
            jsonData[groupId][factorId] = {};
        }
        jsonData[groupId][factorId][typeVal] = jObj.val();
    });
    return JSON.stringify(jsonData);
};

space.saveAnalyze = function () {
    var jsonData = space.getTable();
    if (jsonData) {
        $('#json_data').val(jsonData);
    }
    var data = [];
    $('input[type="hidden"][name]').each(function () {
        if ( $(this).val() ) {
            data.push(encodeURIComponent ( $(this).attr ( 'name' ) ) + '=' + encodeURIComponent ( $(this).val()));
        }
    });
    data.push("type=SPACE");
    $.ajax({
        url: $('#action').val(),
        data: data.join('&') ,
        dataType: 'json',
        type: 'POST',
        success: function (response) {
            console.log(response);
            if (response.result == 'OK') {
                $('#id').val(response.id);
                if (response.analyze_url) {
                    window.location.replace(response.analyze_url);
                }
                else {
                    space.showResults();
                }
            }
        },
        error: function (response) {
            console.log(response);
            alerts.set("Что-то сломалось", "danger");
        }
    });
    return true;
};

space.showResults = function() {
    var negX = parseFloat($(".group_mark[data-group-id=competive]").text()),
        posY = parseFloat($(".group_mark[data-group-id=finance]").text()),
        posX = parseFloat($(".group_mark[data-group-id=industry]").text()),
        negY = parseFloat($(".group_mark[data-group-id=stability]").text()),
        vectX = negX + posX,
        vectY = negY + posY;

    $("#step1").addClass("hidden");
    $("#step2").removeClass("hidden");
    $("#drawing").empty();
    $("#sidebar a[data-step-id=step1]").removeClass("active");
    $("#sidebar a[data-step-id=step2]").addClass("active");
    draw(negX, posY, posX, negY, vectX, vectY, 'drawing');
};

space.init = function () {
    if ($('#json_data').val()) {
        space.makeTable($('#json_data').val());
        space.showResults();
    }

    $("input[data-type='weight']").keyup(function (event) {
        event.stopPropagation();
        var jqueryObj = $(this);
        space.reCalcAbsMark(jqueryObj.attr('data-factor-id'));
        space.setCalcWeight(jqueryObj.attr('data-group-id'));
        space.setGroupMark(jqueryObj.attr('data-group-id'));
        if (!space.checkSumWeight(jqueryObj.attr('data-group-id'))) {
            $('#send_btn').attr("disabled", "disabled");
        } else {
            $('#send_btn').removeAttr("disabled");
        }
    });
    $("select[data-type='mark']").change(function () {
        var jqueryObj = $(this);
        space.reCalcAbsMark(jqueryObj.attr('data-factor-id'));
        space.setGroupMark(jqueryObj.attr('data-group-id'));
    });

    $("#sidebar a").click(function(e){
        e.stopPropagation();
        var stepId = $(this).attr("data-step-id"),
            stepElementToShow = $("#"+stepId);
        if (stepId == "step2") {
            if ($("#id").val() == "") {
                alerts.set("Для отображения результата, сохраните анализ!", "danger");
                return false;
            }
            space.showResults();

        }
        $("#sidebar a").removeClass("active");
        $(this).addClass("active");
        $(".step").addClass("hidden");
        stepElementToShow.removeClass("hidden");
    });
    $("#send_btn").click(function (event) {
        event.stopPropagation();
        space.saveAnalyze();
    });
};


