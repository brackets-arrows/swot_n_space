$(document).ready(function(){

	/*
		negX - КОНКУРЕНТНОЕ ПРЕИМУЩЕСТВО
		posY - ФИНАНСОВОЕ ПОЛОЖЕНИЕ
		posX - ПРИВЛЕКАТЕЛЬНОСТЬ ОТРАСЛИ
		negY - СТАБИЛЬНОСТЬ ВНЕШНЕЙ СРЕДЫ

	*/
});
function draw(negX, posY, posX, negY, vecX, vecY, elemId) {
    var coords = [negX, posY, posX, negY],
        max = Math.ceil(coords.map(function(item){ return Math.abs(item);}).max()),
        axisMax = max + 1;
    $.jqplot (elemId, [
        [[negX, 0], [0, posY]],
        [[posX, 0], [0, negY]],
        [[0, posY], [posX, 0]],
        [[negX, 0], [0, negY]],
        [[0, 0], [vecX, vecY]],
        [[-axisMax, 0], [axisMax, 0]],
        [[0, axisMax], [0, -axisMax]]],
        {
            axes:{
                xaxis:{show: true, min: -max, max: max},
                yaxis:{show: true, min: -max, max: max}
            }
        });
mytitle = $('<div class="my-jqplot-title1 jqp-title">Консервативное</div>').insertAfter('.jqplot-grid-canvas');
mytitle = $('<div class="my-jqplot-title2 jqp-title">Агрессивное</div>').insertAfter('.jqplot-grid-canvas');
mytitle = $('<div class="my-jqplot-title3 jqp-title">Оборонительное</div>').insertAfter('.jqplot-grid-canvas');
mytitle = $('<div class="my-jqplot-title4 jqp-title">Конкурентное</div>').insertAfter('.jqplot-grid-canvas');
}
