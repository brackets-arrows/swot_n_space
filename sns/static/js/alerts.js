var alerts = {

    init: function() {
        this.attrs = {
            baseAlert: $("#base_alert"),
            alertsWrapper: $("#alerts"),
            alertsIds: [0],
            types: ["success", "warning", "info", "danger"]
        };
    },

    set: function(text, type) {
        if (!(this.attrs.types.indexOf(type) + 1)) {
            return undefined;
        }
        var newAlert = this.attrs.baseAlert.clone(),
            newId =  this.attrs.alertsIds.max()+1;
        newAlert.addClass("alert-"+type).removeAttr("id").find("span.text").text(text);
        newAlert.attr("data-id", newId).appendTo(this.attrs.alertsWrapper);
        this.attrs.alertsIds.push(newId);

        newAlert.delay(6000).fadeOut(1000);

        return newId;
    },

    clean: function() {
        this.attrs.alertsWrapper.find(".alert").not("#base_alert").remove();
    },

    removeAlert: function(id) {
        this.attrs.alertsWrapper.find(".alert[data-id="+id+"]").remove();
        this.attrs.alertsIds.splice(this.attrs.alertsIds.indexOf(id), 1);
    }
};

alerts.init();