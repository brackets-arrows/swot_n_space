/**
 * Created with PyCharm.
 * User: alef
 * Date: 09.03.15
 * Time: 1:29
 * To change this template use File | Settings | File Templates.
 */
var swot1 = {};
swot1.kind = 'SWOT';
swot1.step = '1';
swot1.table = $('#swot_table_1');
swot1.addBtn = '[data-type-elem="add_factor"]';
swot1.addFactor = function (typeId, groupId, value) {
    var group_type = '[data-type-id="' + typeId + '"][data-group-id="' + groupId + '"]';
    var addBtn = 'button' + group_type + swot1.addBtn;
    console.log(addBtn);
    var tr = $('<tr/>');
    var td = $('<td/>');
    var tdDel = td.clone();
    var dataInput = {
        type: "text",
        "data-type-id": typeId,
        "data-group-id": groupId,
    };
    if (value) {
        dataInput.value = value;
    }
    var factor =  $('<input/>',dataInput);
    var rmBtn = $('<button/>',{
        "data-type-id": typeId,
        "data-group-id": groupId,
        "data-type-elem":"del_factor",
        click: function (event) {
             event.stopPropagation();
            $(this).parent().parent().remove();
        }
    });
    rmBtn.html('удалить фактор');
    td.append(factor);
    tdDel.append(rmBtn);
    tr.append(td);
    tr.append(tdDel);
    //console.log(tr);
    $('table'+ group_type).append(tr);
};

swot1.resumeAnalyze = function () {
    // здесь андизабл
};

swot1.stopAnalyze = function () {
    // здесь дизабл
};

swot1.successFunc = function (response) {
    $('a[data-step]').each(function (){
        $(this).attr('href',response.analyze_step[$(this).attr('data-step')]);
    });
};
swot1.failFunc = function (response) {
    console.log(response);
    alerts.set("Что-то сломалось", "danger");
};

swot1.getTable = function () {
    var jsonData = {};
    $('input[type="text"][data-type-id][data-group-id]').each(
        function () {
            var groupId = $(this).attr('data-group-id'), typeId = $(this).attr('data-type-id');
            if (!(groupId in jsonData)) {
                jsonData[groupId] = {};
            }
            if (typeId == 'weight') {
                jsonData[groupId][typeId] = $(this).val();
            } else {
                if (!(typeId in jsonData[groupId])) {
                   jsonData[groupId][typeId] = [];
                }
                if ($(this).val()) {
                    jsonData[groupId][typeId].push($(this).val());
                }
            }
        }
    );
    return jsonData;
};


swot1.makeTable = function (jsonData) {
    console.log(jsonData);
    for (var groupId in jsonData) {
        for (var typeId in jsonData[groupId]) {
            if (typeId == 'weight') {
                $('input[data-type-id="' + typeId + '"][data-group-id="' + groupId + '"]').val(jsonData[groupId][typeId]);
            } else {
                jsonData[groupId][typeId].forEach(function (el) {
                    swot1.addFactor(typeId, groupId, el);
                })
            }
        }
    }
};

swot1.init = function () {
    $(swot1.addBtn).click(function (event){
         event.stopPropagation();
         swot1.addFactor($(this).attr('data-type-id'),$(this).attr('data-group-id'))
    });
    $('input[data-type-id="weight"]').keyup(function () {
       if (parseFloat($(this).val()) < 0 || parseFloat($(this).val()) > 1) {
           common.stopAnalyze("Вес должен быть в диапозоне от 1 до 0");
       } else {
           common.resumeAnalyze();
       }
    });
};