/**
 * Created with PyCharm.
 * User: alef
 * Date: 21.03.15
 * Time: 22:39
 * To change this template use File | Settings | File Templates.
 */

var swot4 = {};


swot4.table = $('#swot_table_4');

swot4.step = '4';

swot4.mapFromPrevStep = function (data) {
    console.log(JSON.stringify(data));
     return data.map(function (pair) {
        return {
            first: pair.first,
            second: pair.second,
            factors: pair.factors
                .sort(function(a,b) {
                    var a_mark = a.mark,
                        b_mark = b.mark;
            	    if (a_mark > b_mark) {
                	return -1;
            	    } else if (b_mark > a_mark) {
                	return 1;
           	    } else {
                	return 0;
            	    }
                }).map(function(factor, idx) {
                    factor.top = Boolean(idx < 5);
                    return factor;
                })
        };
     });
};

swot4.getTable = function () {
    var oldJsonData = $('#json_data').val();
    oldJsonData = JSON.parse(oldJsonData);
    return swot4.mapFromPrevStep(oldJsonData['3']);
};

swot4.makeTable  = function (stepData) {
      stepData.forEach(function (el) {
        var tbody = swot4.table.find('tbody[data-first="' + el.first + '"][data-second="' + el.second + '"]');
        el.factors.forEach(function (factor) {
            var tr = $('<tr/>');
            var tdFirst = $('<td/>');
            var tdSecond = tdFirst.clone();
            tdFirst.attr('data-type-id','first');
            tdSecond.attr('data-type-id','second');
            var tdMark = $('<td/>');
            tdMark.attr('data-type-id','mark');
            tdFirst.html(factor.first);
            tdSecond.html(factor.second);
            tdMark.html(factor.mark);
            tr.append(tdFirst);
            tr.append(tdSecond);
            tr.append(tdMark);
            if (factor.top) {
                tr.addClass('red');
            }
            tbody.append(tr);
        });
    });
};

swot4.successFunc = function () {

};

swot4.failFunc = function () {};


swot4.init = function () {
    common.saveAnalyze(swot4);
};
