/**
 * Created with PyCharm.
 * User: alef
 * Date: 14.03.15
 * Time: 23:11
 * To change this template use File | Settings | File Templates.
 */


var swot2 = {};

swot2.table = $('#swot_table_2');

swot2.step = '2';

swot2.getTable = function () {
    var jsonData = {};
    $('tbody[data-group-id]').each(function () {
         var group = [];
         $(this).find('tr').each(function () {
                var weight = parseFloat($(this).children('td[data-type-id="weight"]').html()), mark = parseInt($(this).children('td[data-type-id="mark"]').children('select').val());
                group.push({
                    factor: $(this).children('td[data-type-id="factor"]').html(),
                    weight: weight,
                    mark: mark,
                    total_mark: weight * mark
                });
             }
         );
        jsonData[$(this).attr('data-group-id')] = group;
    });
    console.log(jsonData);
    return jsonData;
};
swot2.calcTotalMark = function (select) {
    var mark = select.val(),td = select.parent(), weight = td.siblings('td[data-type-id="weight"]').html();
    td.siblings('td[data-type-id="total_mark"]').html(parseInt(mark) * parseFloat(weight));
};

swot2.makeTable = function (stepData) {
    for (var table in stepData) {
        stepData[table].forEach(function (el) {
            var tbody = $('tbody[data-group-id="' + table + '"]');
            var tr = $('<tr/>');
            var td = $('<td/>');
            var tdWeight = td.clone();
            tdWeight.attr('data-type-id','weight');
            var tdMark = td.clone();
            tdMark.attr('data-type-id','mark');
            var tdCalcMark = td.clone();
            tdCalcMark.attr('data-type-id','total_mark');
            td.html(el.factor);
            td.attr('data-type-id','factor');
            tdWeight.html(el.weight);
            var dataInput = {
                "data-type-id": 'mark',
                "data-group-id": table,
                change: function (event) {
                    event.stopPropagation();
                    var select = $(this);
                    swot2.calcTotalMark(select);
                }
            };
            if ('mark' in el) {
                dataInput.value = el.mark;
            }
            var mark =  $('<select/>',dataInput);
            [1,5,10].forEach(function (m) {
               var opt = $('<option/>',{value: m});
                opt.html(m);
                mark.append(opt);
            });
            tdMark.append(mark);
            tr.append(td);
            tr.append(tdWeight);
            tr.append(tdMark);
            tr.append(tdCalcMark);
            tbody.append(tr);
            swot2.calcTotalMark(mark);
        });
    }
};

swot2.mapFromPrevStep = function (data) {
    var stepData = {
        'S': [],
        'W': [],
        'O': [],
        'T': []
    };
    for (var key in data) {
        for (var type in stepData) {
            if (type in data[key]){
                var weight = data[key]['weight'];
                stepData[type] = stepData[type]
                    .concat(data[key][type]
                    .map(function (el) {
                            return {factor: el, weight: weight};
                        }
                    )
                );
            }
        }
    }
    return stepData;
};

swot2.successFunc = function () {};

swot2.failFunc = function () {
    alerts.set("Чтото пошло не так", "danger");
};

swot2.init = function () {

};