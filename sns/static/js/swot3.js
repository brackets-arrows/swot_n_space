/**
 * Created with PyCharm.
 * User: alef
 * Date: 19.03.15
 * Time: 17:16
 * To change this template use File | Settings | File Templates.
 */
var swot3 = {};

swot3.table = $('#swot_table_3');

swot3.step = '3';

swot3.getPairFromPage = function () {
    var pair = [];
    swot3.table.find('tbody[data-first]').each(function (){
       pair.push([$(this).attr('data-first'),$(this).attr('data-second')]);
    });
    return pair;
};

swot3.getTable = function () {
    var jsonData = [];
    swot3.table.find('tbody[data-first][data-second]').each(function () {
        var factors = [];
        $(this).children().each(function () {
            factors.push({
                first: $(this).children('[data-type-id=first]').html(),
                second: $(this).children('[data-type-id=second]').html(),
                mark: $(this).find('select[data-type-id=mark]').val()
            });
        });
        jsonData.push({
            first: $(this).attr('data-first'),
            second: $(this).attr('data-second'),
            factors: factors
        });
    });
    return jsonData;
};

swot3.makeTable = function (stepData) {
    stepData.forEach(function (el) {
        var tbody = swot3.table.find('tbody[data-first="' + el.first + '"][data-second="' + el.second + '"]');
        el.factors.forEach(function (factor) {
            var tr = $('<tr/>');
            var tdFirst = $('<td/>');
            var tdSecond = tdFirst.clone();
            tdFirst.attr('data-type-id','first');
            tdSecond.attr('data-type-id','second');
            var tdMark = $('<td/>');
            tdMark.attr('data-type-id','mark');
            tdFirst.html(factor.first);
            tdSecond.html(factor.second);
            var dataInput = {
                "data-type-id": 'mark'
            };
            var mark =  $('<select/>',dataInput);
            [1,2,3,4,5].forEach(function (m) {
               var opt = $('<option/>',{value: m});
                opt.html(m);
                mark.append(opt);
            });
            if ('mark' in factor) {
                mark.val(el.mark);
            }
            tdMark.append(mark);
            tr.append(tdFirst);
            tr.append(tdSecond);
            tr.append(tdMark);
            tbody.append(tr);
        });
    });
};
swot3.mapFromPrevStep = function (data) {
    var stepData = {};
    for (var group in data) {
        stepData[group] = data[group].sort(function (a,b) {
	    var a_mark = a.total_mark,
		b_mark = b.total_mark;
            if (a_mark > b_mark) {
                return -1;
            } else if (b_mark > a_mark) {
                return 1;
            } else {
                return 0;
            }
        }).slice(0, 5);
    }
    var pairs = swot3.getPairFromPage();
    console.log(JSON.stringify(pairs));
    var pairData = pairs.map(function (pair) {
        console.log(pair);
        var pairFactor = [];
        stepData[pair[0]].forEach(function (factorFromFirstGroup) {
            stepData[pair[1]].forEach(function (factorFromSecondGroup) {
                pairFactor.push({
                    first: factorFromFirstGroup.factor,
                    second:factorFromSecondGroup.factor
                });
            });
        });
        return {
            first: pair[0],
            second: pair[1],
            factors: pairFactor
        };
    });
    return pairData;
};

swot3.successFunc = function () {};

swot3.failFunc = function () {
    alerts.set("Что то пошло не так", "danger");
};

swot3.init = function () {

};

