Array.prototype.max = function () {
    return Math.max.apply(null, this);
};

var common = {};
common.fields = 'input:not([type="button"],[type="submit"]),' +
    'select,' +
    'textarea';
common.getFormData = function (form) {
    console.log(form);
    var formData = [];
    $(form).find(common.fields).each(function () {
        formData.push(encodeURIComponent($(this).attr('name')) + '=' + encodeURIComponent($(this).val()));
    });
    return formData.join('&');
};

common.saveAnalyze = function (analyze,extraFunc) {
    var jsonData = analyze.getTable();
    var oldJsonData = $('#json_data').val();
    if (jsonData) {
        if (oldJsonData) {
            oldJsonData = JSON.parse(oldJsonData);
            for (var step in oldJsonData) {
                if (parseInt(step) > parseInt(analyze.step)) {
                    delete oldJsonData[step];
                    $('a[data-step="' + step + '"]').addClass('disabled');
                }
            }
        } else {
            oldJsonData = {};
        }
        oldJsonData[analyze.step] = jsonData;
        oldJsonData = JSON.stringify(oldJsonData);
        $('#json_data').val(oldJsonData);
    }
    console.log(oldJsonData);
    var data = [];
    $('input[type="hidden"][name]').each(function () {
        if ($(this).val()) {
            data.push(encodeURIComponent($(this).attr('name')) + '=' + encodeURIComponent($(this).val()));
        }
    });
    data.push("type=" + analyze.kind);
    $.ajax({
        url: $('#action').val(),
        data: data.join('&'),
        dataType: 'json',
        type: 'POST',
        success: function (response) {
            console.log(response);
            if (response.result == "OK") {
                $('#id').val(response.id);
                var nextStep =  parseInt(analyze.step) + 1;
                $('a[data-step="' + nextStep + '"]').removeClass('disabled');
                analyze.successFunc(response);
                if (extraFunc) {
                    extraFunc(response);
                }
            } else {
                analyze.failFunc(response);
            }
        },
        error: analyze.failFunc
    });
    return true;
};
common.loadAnalyze = function (analyze) {
    var oldJsonData = $('#json_data').val();
    if (oldJsonData) {
        oldJsonData = JSON.parse(oldJsonData);
        if (analyze.step in oldJsonData) {
            analyze.makeTable(oldJsonData[analyze.step]);
        } else {
            var prevStep = parseInt(analyze.step) - 1;
            if (prevStep in oldJsonData) {
                var dataForStep = analyze.mapFromPrevStep(oldJsonData[prevStep]);
                console.log(dataForStep);
                analyze.makeTable(dataForStep);
            }
        }
    }
};

common.setAnalyzeId = function (response) {
    $('#id').val(response.id);
};
common.stepFromData = function () {
    var oldJsonData = $('#json_data').val();
    var stepFromData;
    if (oldJsonData) {
        stepFromData = Object.keys(JSON.parse(oldJsonData)).max();
    }
    return stepFromData;
};

common.enableSidebar = function (stepAct) {
    var step = parseInt(common.stepFromData()) || 0;
    var max = step + 1;
    $('a[data-step]').each(function () {
        $(this).click(function (event) {
            if ($(this).hasClass('disabled')) {
                event.preventDefault();
            }
        });
        if (parseInt($(this).attr('data-step')) <= max) {
            $(this).removeClass('disabled');
        }
        if ($(this).attr('data-step') == stepAct) {
            $(this).addClass('active');
        }
    });

};

common.checkAnalyzeStep = function () {
    var stepFromReq = $('#step').val(), stepFromData = common.stepFromData();
    if (stepFromReq) {
        if (parseInt(stepFromReq) - parseInt(stepFromData) > 1) {
            window.location = $('analyze_url').val();
        } else {
            return stepFromReq;
        }
    } else {
        if (parseInt(stepFromData) < 4) {
            return parseInt(stepFromData) + 1;
        } else {
            return stepFromData || '1';
        }
    }
};

common.resumeAnalyze = function (msg) {
     $('#send_btn').removeAttr("disabled");
    if (msg) {
        alerts.set(msg, 'success');
    }
};

common.stopAnalyze = function (msg) {
     $('#send_btn').attr("disabled", "disabled");
     alerts.set(msg, 'danger');
};

if ($('#company_form').length) {
    company.init();
}
if ($('#space_tbl').length) {
    space.init();
}
if ($('#swot_table_1').length) {
    var step = common.checkAnalyzeStep();
    console.log(step);
    var mapStep = {
        '1': swot1,
        '2': swot2,
        '3': swot3,
        '4': swot4
    };

    var analyze = mapStep[step];
    analyze.table.removeClass('hidden');
    if (analyze.step == '4') {
        $('#send_btn').addClass('hidden');
        alerts.set("Анализ успешно закончен", "success");
    }
    common.loadAnalyze(analyze);
    $('#send_btn').click(function (event) {
        event.stopPropagation();
        common.saveAnalyze(analyze,function (res) { window.location = res.analyze_url; });
    });
    common.enableSidebar(step);

    analyze.init()
}