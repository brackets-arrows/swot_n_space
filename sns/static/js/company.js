/**
 * Created with PyCharm.
 * User: alef
 * Date: 04.03.15
 * Time: 16:45
 * To change this template use File | Settings | File Templates.
 */

var company = {};
company.init = function () {
   $('#company_form').submit(function (event){
       event.stopImmediatePropagation();
         $.ajax({
				url: this.action,
				data: common.getFormData ( this ),
				dataType: 'json',
				type: this.method,
				success: function ( response ) {
                    console.log(response);
                    if (response.result == 'OK') {
                        $('#id').val(response.id);
                        alerts.set("Компания успешно создана!", "success");
                        window.location = '/company/' + response.id;
                    }
				},
                error: function (response) {
                    console.log(response);
                    alerts.set("Что-то сломалось", "danger");
                }
			});
       return false;

   });
};