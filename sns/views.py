import json

from django.views.generic import RedirectView, ListView, FormView
from django.http import HttpResponse
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login

from sns.models import *
from sns.forms import RegisterUserForm


def save_company(request):
    company_id = request.POST.get("id")
    print request.user
    if company_id:
        Company.objects.filter(id=company_id).update(
            description=request.POST.get("description"),
            name=request.POST.get("name"),
        )
    else:
        company = Company(owner=request.user, name=request.POST.get("name"), description=request.POST.get("description"))
        company.save()
        company_id = company.id
    return HttpResponse(json.dumps({'result': 'OK', 'id': company_id}), content_type='application/json')


def save_analyze(request):
    analyze_id = request.POST.get("id")
    json_data = request.POST.get("json_data")
    dec_json_data = json.loads(json_data)
    if analyze_id is not None:
        Analysis.objects.filter(id=analyze_id).update(json_data=dec_json_data)
        obj = Analysis(id=analyze_id, json_data=dec_json_data)
    else:
        company_id = request.POST.get("company_id")
        company = Company(id=company_id)
        obj = Analysis(
            company=company,
            type=request.POST.get("type"),
            json_data=dec_json_data
        )
        obj.save()
        analyze_id = obj.id
    url = obj.get_absolute_url()
    urls_step = obj.get_absolute_urls()
    return HttpResponse(json.dumps({'result': 'OK', 'id': analyze_id,
                                    "analyze_url": url, "analyze_step": urls_step}), content_type='application/json')


def company(request, company_id=None):
    context = {}
    if company_id is not None:
        context['comp'] = Company.objects.get(id=company_id)
        context['analysis'] = context['comp'].analysis_set.all()
    return render(request, 'company.html', context)


def analyze(request, type_analyze=None, analyze_id=None, step=None, company_id=None):
    context = {'company_id': company_id}
    if step is not None:
        context['step'] = step
    if analyze_id is not None:
        analyze_obj = Analysis.objects.get(id=analyze_id)
        context['analyze'] = analyze_obj
        type_analyze = analyze_obj.type
        context['json_data'] = json.dumps(analyze_obj.json_data)
        context['company_id'] = analyze_obj.company.id
        context['analyze_url'] = analyze_obj.get_absolute_url()
        context['analyze_url_steps'] = analyze_obj.get_absolute_urls()
    if type_analyze == 'SWOT':
        table_1 = [{'group_id': group, 'group_name': TELESCOPIC[group]} for group in TELESCOPIC.keys()]
        context['table_1_columns'] = zip(*[iter(table_1)] * 1)
        context['table_1_rows'] = map(lambda key: {'id': key, 'name': SWOT_1[key]}, SWOT_1.keys())
        context['tables_2'] = map(lambda key: {'id': key, 'name': SWOT_2[key]}, SWOT_2.keys())
        context['table_2_columns'] = SWOT_2_col
        context['table_3'] = SWOT_3_tables
    else:
        context['table_1_rows'] = SPACE_1
    return render(request, 'analyze_' + type_analyze + '.html', context)


class CompaniesList(ListView):
    model = Company
    template_name = "companies_list.html"

    def get_queryset(self):
        return self.request.user.company_set.all()


class RegisterView(FormView):
    template_name = "register.html"
    form_class = RegisterUserForm
    success_url = "/"

    def form_valid(self, form):
        uname = form.cleaned_data.get("username")
        paswd = form.cleaned_data.get("password")
        User.objects.create_user(username=uname, password=paswd)
        auth_user = authenticate(username=uname, password=paswd)
        if auth_user is not None:
            login(self.request, auth_user)
        return super(RegisterView, self).form_valid(form)


class HomeView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        if self.request.user.is_anonymous():
            return reverse("login")
        else:
            return reverse("companies_list")